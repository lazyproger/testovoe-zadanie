export const state = () => ({
  posts: [],
  authors: {},
})

export const actions = {
  async getPosts({ commit }) {
    const posts = await this.$axios.$get(
      'https://jsonplaceholder.typicode.com/posts'
    )
    commit('setPosts', posts)
  },
}

export const mutations = {
  setPosts(state, posts) {
    state.posts = posts
  },
}

export const getters = {
  posts: (state) => {
    return state.posts
  },
}
